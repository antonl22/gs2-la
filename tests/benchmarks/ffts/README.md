Time nonlinear terms
--------------------

This test times repeated calls to the nonlinear term. The number of iterations
is controlled by nstep, default 10.
