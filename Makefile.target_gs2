gs2_all: diagnostics modules gs2 ingen rungridgen generate_fftw_wisdom

#Main code
gs2: $(gs2_mod) $(SIMPLEDATAIO_LIB)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS) 

#Main code, may not work as missing SIMPLEDATAIO_LIB dependency
gs2.x: $(gs2_mod)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

#Input file checker
ingen: $(ingen_mod) $(SIMPLEDATAIO_LIB)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS) 

#Utility to produce fftw wisdom files for given problem size
generate_fftw_wisdom: $(generate_fftw_wisdom_mod) $(SIMPLEDATAIO_LIB)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS) 

#Calculates the field response matrix for current input file and writes to file
dump_response: $(dump_response_mod)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS) 

#Initialises theta, kx, ky, lambda and energy grids and writes to file
dump_grids: $(dump_grids_mod)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS) 

#Calculates the ideal ballooning stability (optionally over a scan in shat/beta_prime)
ideal_ball: $(ideal_ball_mod)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS) 

#Driver program for gridgen?
rungridgen: $(rungridgen_mod)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

#A simple program to test linear regression on random data || Not at all gs2 specific!
regress: $(drive_mod)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

#A program which test the fft transforms used in GS2
ffttester: $(ffttester_mod)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

.PHONY: modules utils_all geo_all libgs2_all

# Order-only prerequisites need to be specified separately for this to
# work properly
modules: | $(UTILS_SENTINEL)
modules: utils.a geo.a libgs2.a diagnostics

### UTILS
utils_all: | $(UTILS_SENTINEL)
utils_all: utils.a

UTIL_OBJ = spl.o constants.o file_utils.o netcdf_utils.o command_line.o runtime_tests.o constant_random.o
utils.a: | $(UTILS_SENTINEL)
utils.a: $(UTIL_OBJ)
	$(ARCH) $(ARCHFLAGS) $@ $^
	$(RANLIB) $@

### GEO
geo_all: geo.a eiktest ball

GEO_OBJ = geometry.o geq.o eeq.o peq.o leq.o deq.o ideq.o ceq.o read_chease.o

geo.a: $(GEO_OBJ)
	$(ARCH) $(ARCHFLAGS) $@ $^
	$(RANLIB) $@

geo_c: geometry_c_driver.o ../geo.a ../utils.a  
	$(FC) -o  geo_c geometry_c_driver.o ../geo.a ../utils.a -Mnomain 

geo_c.a: geometry_c_interface.o ../geo.a ../utils.a  
	$(FC) -o  geo_c2.a geometry_c_interface.o ../geo.a ../utils.a -Mnomain -Mnostartup -Mnostdlib

#Program to calculate ideal MHD ballooning stability
ball: $(ball_mod)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

#Program to calculate eikcoeffs (geometry coefficients etc.) and write to file
eiktest: $(eiktest_mod)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

### LIBGS2
libgs2_all: libgs2.a

libgs2.a: $(filter-out gs2.o simpledataiof.o simpledataio_write.o, $(gs2_mod)) gs2_gryfx_zonal.o
	$(ARCH) $(ARCHFLAGS) $@ $^
	$(RANLIB) $@

distclean:
	-rm -f gs2 gs2.x
	-rm -f ingen rungridgen regress
	-rm -f ball eiktest
	-rm -f ffttester
	-rm -f dump_response
	-rm -f dump_grids
	-rm -f ideal_ball
	-rm -f generate_fftw_wisdom
