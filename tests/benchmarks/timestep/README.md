Advance time benchmark
----------------------

This test times the whole gs2 time step. The number of iterations is controlled
by nstep, default 10.
