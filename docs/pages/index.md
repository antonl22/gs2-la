title: Notes

This is the front page to a collection of different notes etc. that are written in markdown etc.
This is a place in which we can discuss examples, how to compile, how to contribute etc. as well
as providing high level documentation of the code etc.

